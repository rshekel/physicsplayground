function p1(m,M,T,x0,v0,y0,w0)
[tsol, xsol]=ode23(@(t,x)p1mM(t,x,m,M), [0 T], [x0,v0,y0,w0]); 
plot(xsol(:,1), xsol(:,3));
% Copied from ruth lawrence 
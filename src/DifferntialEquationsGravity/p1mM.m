function dxdt = p1mM(t,x,m,M)
dxtwodt = -m*x(1)*(x(1)^2+x(3)^2)^(-1.5)  -M*(x(1)-1)*((x(1)-1)^2+x(3)^2)^(-1.5); 
dxfourdt = -m*x(3)*(x(1)^2+x(3)^2)^(-1.5) -M*(x(3)-1)*((x(1)-1)^2+x(3)^2)^(-1.5); 
dxdt = [x(2); dxtwodt;x(4);dxfourdt];
% Copied from ruth lawrence 
import re
import os
import sys
import random
import requests

def main():
    coursenum = input("What course number do you want? ").encode('UTf-8')
    start_year = input("From what year? ").encode('UTf-8')
    end_year = input("Till what year? ").encode('UTf-8')

    params = {b'coursenum': coursenum, 
            b'action'   : b"mode2", 
            b"year2"    : end_year, 
            b"year1"    : start_year, 
            b"moed"     : b"0", 
            b"Submit"   : rb"%F9%EC%E7"
            }

    ans = requests.post(r'http://www4.huji.ac.il/htbin/exams/exams.cgi', params)
    data = ans.text
    test_urls = re.findall('http.*?pdf', data)

    if not test_urls:
        print("Sorry, no tests found...")
        sys.exit(1)

    tests_dir = os.path.join(os.environ['homepath'], 'Downloads', f'{coursenum.decode("utf-8")}_{str(random.randint(100,1000))}')
    os.mkdir(tests_dir)
    print(f"Will save to directory {tests_dir}")
    for url in test_urls:
        print(f"Fetching {url}...")
        test = requests.get(url)
        filename = re.findall('20.*pdf', url)[0]
        open(os.path.join(tests_dir, filename), 'wb').write(test.content)

if __name__ == "__main__":
    main()


import gui
from consts import DEFAULT_ACCURACY, DEFAULT_POINT


def check_theory(point, accuracy=DEFAULT_ACCURACY):
    """
    assuming my point is the beginning of axis table, i'll call myself always (0,0)
    """
    gui.paint(point, accuracy)
    gui.run()


if __name__ == "__main__":
    check_theory(DEFAULT_POINT)

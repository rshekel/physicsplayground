from circular_electric_field import Point

START_X = 300
START_Y = 300

DEFAULT_ACCURACY = 80
DEFAULT_POINT = Point(20, 18)
CANVAS_PARAMS = {'width': 1000, 'height': 1000}
DOT_WIDTH = 0.1

DRAW_POWER = 200000
CENTER = Point(2, 4)
DEFAULT_RADIUS = 50
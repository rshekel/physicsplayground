from Tkinter import Canvas, Tk, mainloop
from consts import START_X, START_Y
from consts import CENTER, CANVAS_PARAMS, DOT_WIDTH
from consts import DRAW_POWER, DEFAULT_RADIUS, DEFAULT_ACCURACY
from circular_electric_field import calc_force, generate_points_on_circle
from point import Point

master = Tk()

canvas = Canvas(master, **CANVAS_PARAMS)
canvas.pack()


def draw_dot(canvas, point, width=DOT_WIDTH, fill=None):
    """
    :type canvas: Canvas
    """
    assert isinstance(canvas, Canvas)
    x0, y0 = point.x - width + START_X, point.y - width + START_Y
    x1, y1 = point.x + width + START_X, point.y + width + START_Y
    canvas.create_oval(x0, y0, x1, y1, fill=fill)


def draw_power(canvas, start, vector, power=DRAW_POWER):
    assert isinstance(canvas, Canvas)
    canvas.create_line(start.x + START_X, start.y + START_Y, START_X + start.x + (vector.x * power), START_Y + start.y + (vector.y * power), fill="red")


def paint(point, accuracy):
    draw_dot(canvas, CENTER, width=3, fill="green")

    circle_points = generate_points_on_circle(radius=DEFAULT_RADIUS, center=CENTER, amount_of_points=accuracy)

    x_force = 0
    y_force = 0

    for circ_point in circle_points:
        x, y = calc_force(point, circ_point)
        draw_power(canvas, point, Point(x, y))

        x_force += x
        y_force += y

    for pnt in circle_points:
        draw_dot(canvas, pnt)

    draw_dot(canvas, point, 3, fill='pink')
    canvas.create_text(100, 10, text="x_force: " + str(x_force))
    canvas.create_text(100, 30, text="y_force: " + str(y_force))
    canvas.create_text(100, 50, text="x_error: " + str((x_force - 0) / accuracy))
    canvas.create_text(100, 70, text="y_error: " + str((y_force - 0) / accuracy))

    print "x_force: ", x_force
    print "y_force: ", y_force
    print "x_error: ", (x_force - 0) / accuracy
    print "y_error: ", (y_force - 0) / accuracy


def repaint(event):
    canvas.delete("all")
    paint(Point(event.x - START_X, event.y - START_Y), DEFAULT_ACCURACY)


def run():
    canvas.bind('<Button-1>', repaint)
    mainloop()

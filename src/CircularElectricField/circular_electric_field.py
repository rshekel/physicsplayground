import math
from math import pi
from scipy.spatial.distance import euclidean as dist
from point import Point

# Purpose: show that inside a circle of positive particels there is no electrical field.
# (Because the integral of all forces zeroes. 
# Achieving this in python and no infinitesimal math, just summing up 100 point on the cicular line.

KOULON = 1


def koulon(d, q1=1.0, q2=1.0):
    """
        Returns Force in stat-koulon units, thus K = 1
        practiclly this is just 1 / d^2...
    """
    return KOULON * ((q1 * q2) / float(d ** 2))


def calc_force(p1, p2):
    """
    calculates force that p2 produces on p1
    :param p1: Point
    :param p2: Point
    :return: x_force, y_force
    """
    d = dist(p1.tup, p2.tup)
    force = koulon(d)
    angle = math.atan2(p2.y - p1.y, p2.x - p1.x)
    force_x = force * math.cos(angle)
    force_y = force * math.sin(angle)

    return force_x, force_y


def generate_points_on_circle(radius, center, amount_of_points=800):
    assert isinstance(center, Point)
    points = []
    for i in range(0, amount_of_points + 1):
        x = math.cos(2 * pi / amount_of_points * i) * radius
        y = math.sin(2 * pi / amount_of_points * i) * radius
        x += center.x
        y += center.y
        points.append(Point(x, y))
    return points



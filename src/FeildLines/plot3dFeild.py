#!/usr/bin/python3
# 
# Plot electric potential
#
import numpy as np
from scipy.optimize    import curve_fit
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.cm as cm
import matplotlib.mlab as mlab
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm


class Q(object):
    def __init__(self, x, y, z, q):
        self.x = x
        self.y = y
        self.z = z
        self.q = q

epsilon_0=1
pai = np.pi

def electric_field(x, y, z, qs):
    if not isinstance(qs, list):
        qs = [qs]
    a = 1
    fctr = 1./(4*pai*epsilon_0)
    
    V = 0
    Ex = 0
    Ey = 0
    Ez = 0
    
    for q in qs:
        qq = q.q
        r = np.sqrt((x - q.x)**2 + (y - q.y)**2 + (z - q.z)**2) # size of Electric field 
        V +=  fctr*qq*(1./r) # Potential Scalar 
        Ex += fctr*qq*((x-q.x) / r**3) # x component of field 
        Ey += fctr*qq*((y-q.y) / r**3) # y component of field 
        Ez += fctr*qq*((z-q.z) / r**3) # z component of field 
        
    return V, Ex, Ey, Ez

    
x_range = 1
y_range = x_range
z_range = x_range

def get_grid():
    # set grid
    delta_x = 0.2
    delta_y = delta_x
    delta_z = delta_x
    xvec = np.arange(-x_range,x_range+delta_x,delta_x)
    yvec = np.arange(-y_range,y_range+delta_y,delta_y)
    zvec = np.arange(-z_range,z_range+delta_z,delta_z)
    Xgrid, Ygrid, Zgrid = np.meshgrid(xvec, yvec, zvec)

    xp = np.exp(np.arange(-5., 0.01, 0.25))
    xm = [-x for x in xp[::-1]]
    zz = [0.]
    levels = np.concatenate((xm,zz,xp))
        
    return Xgrid, Ygrid, Zgrid, levels 
    
Xgrid, Ygrid, Zgrid, levels = get_grid()

def plot_field(qs, normalize=False, length=0.1):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    V, Ex, Ey, Ez = electric_field(Xgrid,Ygrid, Zgrid, qs)

    EE = np.sqrt(Ex**2 + Ey**2 + Ez**2)
    EE = EE.reshape(len(EE)**3)
    EE = cm.ScalarMappable().to_rgba(EE)
    
    EElog=np.log10(EE-EE.min()+1.0)

    ax.quiver(Xgrid, Ygrid, Zgrid, Ex, Ey, Ez, normalize=normalize, length=0.1, color=EE**0.5, cmap="autumn")

    # plt.axis([-x_range, x_range, -y_range, y_range])
    plt.show(block=False)

def plot_4corners(l=2, sign=-1):
    # use with x_range=1, delta_x=0.2
    qs = [Q(-l, -l, 0, 1*sign), Q(l, -l, 0, 1*sign), Q(-l, l, 0, 1*sign), Q(l, l, 0, 1*sign)]
    plot_field(qs, True)

#!/usr/bin/python3
# 
# Plot electric potential
#
import numpy as np
from scipy.optimize    import curve_fit
from matplotlib import pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.cm as cm
import matplotlib.mlab as mlab


class Q(object):
    def __init__(self, x, y, q):
        self.x = x
        self.y = y
        self.q = q

epsilon_0=1
pai = np.pi
qs = [Q(0, 0, 2), Q(0,1,-1), Q(0,-1,-1)]

def electric_field(x,y, qs):
    if not isinstance(qs, list):
        qs = [qs]
    a = 1
    fctr = 1./(4*pai*epsilon_0)
    
    V = 0
    Ex = 0
    Ey = 0
    
    for q in qs:
        qq = q.q
        r = np.sqrt((x - q.x)**2+(y - q.y)**2) # size of Electric field 
        V +=  fctr*qq*(1./r) # Potential Scalar 
        Ex += fctr*qq*((x-q.x)/r**3) # x component of field 
        Ey += fctr*qq*((y-q.y)/r**3) # y component of field 
        
    return V,Ex,Ey


def electric_field_approx(x,y):
    # Exc. 2 q8. 
    # assumes qs = [Q(0, 0, 2), Q(0,1,-1), Q(0,-1,-1)], and using taylor approx. 

    a = 1
    fctr = 1./(4*pai*epsilon_0)
    
    qq = -1
    r = np.sqrt(x**2+y**2) # size of Electric field 
    cos_t = x / r 
    sin_t = y / r 
                
    V =  fctr*qq*(1./r) # Potential Scalar is garbage here 
    Ex = 3*(5* sin_t**2 -1)*fctr*qq* a**2/r**4 *cos_t
    Ey = (15* sin_t**2 -9)*fctr*qq* a**2/r**4 *sin_t
    
            
    return V,Ex,Ey

    
x_range = 1
y_range = x_range

def get_grid():
    # set grid
    delta_x = 0.001
    delta_y = delta_x
    xvec = np.arange(-x_range,x_range+delta_x,delta_x)
    yvec = np.arange(-y_range,y_range+delta_y,delta_y)
    Xgrid, Ygrid = np.meshgrid(xvec,yvec)

    xp = np.exp(np.arange(-5., 0.01, 0.25))
    xm = [-x for x in xp[::-1]]
    zz = [0.]
    levels = np.concatenate((xm,zz,xp))
        
    return Xgrid, Ygrid, levels 
    
Xgrid, Ygrid, levels = get_grid()

def plot_single(qs):
    plt.figure()
    V,Ex,Ey = electric_field(Xgrid,Ygrid, qs)
        
    plt.streamplot(Xgrid, Ygrid, Ex, Ey, 
                   color='r',linewidth=1,density=[0.6,1.]
                   ,maxlength=10.)

    CS = plt.contour(V, levels,
                     origin='lower',colors='grey',
                     linestyles='solid',
                     linewidths=1.,
                     extent=(-x_range, x_range, -y_range, y_range))
    plt.axis([-x_range, x_range, -y_range, y_range])
    plt.show(block=False)

def plot_approx():
    plt.figure()
    _,Ex,Ey = electric_field_approx(Xgrid,Ygrid)
        
    plt.streamplot(Xgrid, Ygrid, Ex, Ey, 
                   color='r',linewidth=0.7,density=[0.5,1.]
                   ,maxlength=10.)

    plt.axis([-x_range,x_range, -y_range, y_range])
    plt.show(block=False)

    
def plot_sub(ax, i1, i2, qs, title):
    V,Ex,Ey = electric_field(Xgrid,Ygrid, qs)
    
    ax[i1,i2].set_title(title)
    
    ax[i1][i2].streamplot(Xgrid, Ygrid, Ex, Ey, 
                   color='r',linewidth=0.7,density=[0.5,1.]
                   ,maxlength=10.)

    CS = ax[i1][i2].contour(V, levels,
                     origin='lower',colors='grey',
                     linestyles='solid',
                     linewidths=1.,
                     extent=(-x_range, x_range, -y_range, y_range))
    ax[i1][i2].axis([-x_range,x_range,-y_range,y_range])



def plot_mul():
    fig, ax = plt.subplots(nrows=2, ncols=2)
    plot_sub(ax, 0, 0, qs[0], "positive charge (+2)")
    plot_sub(ax, 0, 1, qs[1], "negative charge (-1)")
    plot_sub(ax, 1, 0, qs[2], "negative charge (-1)")
    plot_sub(ax, 1, 1, qs, "all three charges together")
    fig.show()
        
def vortex_electric_field(x,y, n=1):
    r = np.sqrt(x**2+y**2) # size of Electric field 
                
    Ex = -y/(r**(n+1))
    Ey = x/(r**(n+1))
                
    return Ex,Ey


def plot_vortex(n=1):
    # Ex5 q1 
    plt.figure()
    Ex,Ey = vortex_electric_field(Xgrid,Ygrid, n=n)
        
    plt.streamplot(Xgrid, Ygrid, Ex, Ey, 
                   color='r',linewidth=0.7,density=[0.5,1.]
                   ,maxlength=10.)

    plt.axis([-x_range,x_range,-y_range,y_range])
    plt.show(block=False)


def plot_4_corners(l=2, sign=-1):
    qs = [Q(-l, -l, 1*sign), Q(l, -l, 1*sign), Q(-l, l, 1*sign), Q(l, l, 1*sign)]
    plot_single(qs)